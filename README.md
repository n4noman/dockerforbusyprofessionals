# DockerForBusyProfessionals

## Step 0
- Install docker via Docker For Desktop.
- Verify Docker is installed `docker --version`

## Step 1

- We will run our first container, a web-server.
- Run the command `docker run -p 8080:80 nginx`
- Docker will first download an *image* of nginx, which it will then use to create container.
<img src="./images/docker-run1.JPG">  

- The argument `-p 8080:80` tells docker to map the port 8080 of host machine i.e. your computer to the container port 80. Port 80 is the port which the webserver runs
- Now you can try to check if the webserver is running by using `http://localhost:8080`. 
- Open another command prompt tab and run `docker container ls` to show all containers running. There is one container running, it is assigned a random name *adoring_curie*.
<img src="./images/docker-cont-ls-1.JPG">

- Note the port. It shows port 8080 is mapped to container port 80
- Lets check how this container was created. It was created from an image. You can check docker images on your machine `docker image ls`
<img src="./images/docker-img-ls-1.JPG">

- Image can be used to create any number of containers. Lets create another. But we need to use different port from host machine. 
- We will use two additional params --name to give it a name and -d to run in background. `docker run -p 8081:80 --name second nginx -d`
- You will notice the command returned immediately. Now if you run `docker container ls`, you should see two containers, one with given name `second`
<img src="./images/docker-cont-ls-2.JPG">

- Stop both containers by using `docker container rm --force <container1-name> <container2-name>`. You need to use `--force` because they are running (alternative is to stop first `docker container stop <container-name>` and then remove)
<img src="./images/docker-cont-rm-1.JPG">

- You can create as many containers as you like from same image. 

## Step 2
- Lets build our own image. To create an image, we create a `Dockerfile` with following lines
```
FROM alpine:latest
CMD echo "Hello Hasan"
```
- This is telling docker to start from `alpine` linux and run a command to output.
- To create image use `docker build -t myimage .` (don't ignore the last dot)
- Now if you check images `docker image ls`, you should see myimage

<img src="./images/docker-img-ls-2.JPG">

- Lets create a container from this image `docker run myimage`. 

- It will create a container, run the command and dies immediately. Therefore you won't find it with `docker container ls`. To see all the containers, you should use `docker container ls --all`

- Lets create another image to run PHP. We already have a docker file for PHP
```
FROM php:7.4-cli
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
CMD [ "php", "./simple.php" ]

```
- This is telling docker to start building my image from an existing image called `php:7.4-cli` 
- Next copy the entire directory (specified by .) into container location `/usr/src/myapp` 
- Next make the directory `/usr/src/myapp` the working directory
- And finally run the php command
- To build image use `docker build -t superphp -f PhpDockerfile .`. We have use `-f` because our filename is different; docker by default assumes `Dockerfile`
- Verify that the image `superphp` is there using `docker image ls`
- Now we will use this image to create containers `docker run superphp`

- Next we will create another image for Node.js application as in this tutorial https://nodejs.org/en/docs/guides/nodejs-docker-webapp

